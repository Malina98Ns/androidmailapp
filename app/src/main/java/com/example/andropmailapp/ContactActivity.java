package com.example.andropmailapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.andropmailapp.Model.Contact;
import com.example.andropmailapp.Servis.ServiceClass;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactActivity extends AppCompatActivity {

    private TextView ime;
    private TextView prezime;
    private TextView email;
    private TextView note;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        Intent intent =getIntent();
        Contact contact =(Contact) intent.getSerializableExtra("contact");

        setTitle(contact.getFirstName()+" " +contact.getLastName());
        ime = (TextView) findViewById(R.id.imeKontakta);
        prezime = (TextView) findViewById(R.id.prezimeKontakta);
        email = (TextView) findViewById(R.id.emailKontakta);
        note = (TextView) findViewById(R.id.noteKontakta);
        android.widget.Toast.makeText(getApplicationContext(),contact.getNote(), Toast.LENGTH_SHORT).show();


        ime.setText(contact.getFirstName());
        prezime.setText(contact.getLastName());
        email.setText(contact.getEmail());
        note.setText(contact.getNote());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.contact, menu);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id == R.id.action_settings) {
            Intent intent = new Intent(ContactActivity.this,SettingsActivity.class);
            startActivity(intent);
        }
        if(id == R.id.action_edit){
            Intent intent1 = getIntent();
            Contact contact = (Contact) intent1.getSerializableExtra("contact");
            Intent intent = new Intent(ContactActivity.this,CreateContactActivity.class);
            intent.putExtra("FromActivitySwap","ContactActivity");
            intent.putExtra("contact",contact);
            startActivity(intent);
        }
        if (id == R.id.action_delete){
            brisanjeKontakta();
        }


        return super.onOptionsItemSelected(item);
    }

    private void brisanjeKontakta(){
        AlertDialog.Builder builder = new AlertDialog.Builder(ContactActivity.this);
        builder.setTitle("Brisanje Kontakta");
        builder.setMessage("Da li ste sigurni?");
        builder.setPositiveButton("Da", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent intent =getIntent();
                Contact contact =(Contact) intent.getSerializableExtra("contact");

                Call<Void> call = ServiceClass.contactService.deleteContact(contact.getId());

                call.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        Intent intent1 =getIntent();
                        Contact contact =(Contact) intent1.getSerializableExtra("contact");
                        Intent intent = new Intent(ContactActivity.this,ContactsActivity.class);
                        intent.putExtra("PrethodniKontakt",contact);
                        startActivity(intent);
                        android.widget.Toast.makeText(getApplicationContext(),"Uspesno obrisan kontakt!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        android.widget.Toast.makeText(getApplicationContext(),"Doslo je do greske!",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        builder.setNegativeButton("Ne", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });

        AlertDialog ad = builder.create();
        ad.show();
    }


    @Override
    public void onBackPressed(){
        super.onBackPressed();
    }
}
