package com.example.andropmailapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.example.andropmailapp.Model.Contact;
import com.example.andropmailapp.Model.Email;
import com.example.andropmailapp.Servis.ContactService;
import com.example.andropmailapp.Servis.EmailService;
import com.example.andropmailapp.Servis.ServiceClass;
import com.google.android.material.snackbar.Snackbar;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CreateEmailActivity extends AppCompatActivity {

    private static int TIME_OUT = 2000;
    public static List<Contact> kontakti = new ArrayList<>();
    private List<Email> emailovi = new ArrayList<>();
    private EditText kome;
    private EditText naslov;
    private EditText poruka;
    private Contact trenutni;
    private Contact primalac;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_email);


        final SharedPreferences myPrefs = getSharedPreferences(LoginActivity.Pref, MODE_PRIVATE);
        final String ipAdrs=myPrefs.getString(LoginActivity.Username, "");

        kome = findViewById(R.id.editText14);
        naslov = findViewById(R.id.editText4);
        poruka = findViewById(R.id.editText5);

        String EmailHolder = getIntent().getStringExtra("email");
        String SubjectHolder = getIntent().getStringExtra("emailNaslov");
        String ContentHolder = getIntent().getStringExtra("emailSadrzaj");
        String action = getIntent().getStringExtra("action");

        if(action.equals("reply")){
            setTitle("Odgovaranje");
            EditText emailKorisnika = (EditText)findViewById(R.id.editText14);
            EditText naslovPoruke = (EditText)findViewById(R.id.editText4);

            naslovPoruke.setText("[Re] " + SubjectHolder);
            emailKorisnika.setText(EmailHolder);
        }else if(action.equals("forward")){
            setTitle("Prosledjivanje");
            EditText naslovPoruke = (EditText)findViewById(R.id.editText4);
            EditText sadrzajPoruke = (EditText)findViewById(R.id.editText5);

            naslovPoruke.setText(SubjectHolder);
            sadrzajPoruke.setText(ContentHolder);
        }else if(action.equals("sent")){
            setTitle("Slanje");
        }

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(CreateEmailActivity.this,EmailsActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.create_email, menu);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id == R.id.action_settings) {
            startActivity(new Intent(CreateEmailActivity.this,SettingsActivity.class));
        }
        if(id == R.id.action_send){

            String komeText = kome.getText().toString().trim();
            String naslovText = naslov.getText().toString().trim();
            String porukaText = poruka.getText().toString().trim();


            SharedPreferences myPrefs = getSharedPreferences(LoginActivity.Pref, MODE_PRIVATE);
            String ipAdrs=myPrefs.getString(LoginActivity.Username, "");

            Date currentDate = new Date(System.currentTimeMillis());

            Email email = new Email();
            email.setFrom(ipAdrs);
            email.setTo(komeText);
            email.setContent(porukaText);
            email.setSubject(naslovText);
            email.setDateTime(null);
            email.setProcitano(false);

            kreiranjeEmaila(email, ipAdrs);


            Snackbar snackbar = Snackbar.make(findViewById(R.id.createEmail), "Poruka je poslata!", Snackbar.LENGTH_LONG);
                snackbar.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(CreateEmailActivity.this, EmailsActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, TIME_OUT);


        }
        if(id == R.id.action_cancel){
            Intent intent = new Intent(CreateEmailActivity.this, EmailsActivity.class);
            startActivity(intent);
        }


        return super.onOptionsItemSelected(item);
    }

    public void kreiranjeEmaila(Email createEmail, String username){
        Retrofit retrofit1 = new Retrofit.Builder()
                .baseUrl(ServiceClass.SERVICE_API_PATH)
                .addConverterFactory(GsonConverterFactory.create())
                .client(ServiceClass.test())
                .build();

        EmailService emailService1 = retrofit1.create(EmailService.class);
        Call<Email> call = emailService1.createEmail(createEmail, username);

        call.enqueue(new Callback<Email>() {
            @Override
            public void onResponse(Call<Email> call, Response<Email> response) {
            }

            @Override
            public void onFailure(Call<Email> call, Throwable t) {
                //nista
            }
        });
    }

}
