package com.example.andropmailapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.andropmailapp.Model.Attachment;
import com.example.andropmailapp.Model.Email;
import com.example.andropmailapp.Model.Tag;
import com.example.andropmailapp.Servis.ServiceClass;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EmailActivity extends AppCompatActivity {

    private TextView posiljalac;
    public static TextView naslovPoruke;
    public static String naslov;
    public static String sadrzaj;
    private TextView poruka;
    public static TextView vreme;
    private ListView listaA;
    private ArrayList<String> prikaz = new ArrayList<>();
    public static String emailKor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Intent intent = getIntent();
        Email email =(Email)intent.getSerializableExtra("email");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);
        setTitle(email.getSubject());

        emailKor = email.getFrom();
        naslov = email.getSubject();
        sadrzaj = email.getContent();

        posiljalac = (TextView) findViewById(R.id.posiljalac);
        naslovPoruke = (TextView) findViewById(R.id.naslovPoruke);
        poruka = (TextView) findViewById(R.id.poruka);
        vreme = (TextView) findViewById(R.id.vreme);
        listaA =(ListView) findViewById(R.id.listaAtts);


        posiljalac.setText(email.getFrom());
        naslovPoruke.setText(email.getSubject());
        poruka.setText(email.getContent());
        vreme.setText(stringDatum(email.getDateTime()));

   /*     if (email.getIdAttachmentId()!=null){
            for (Attachment attachment:email.getIdAttachmentId()) {
                prikaz.add(attachment.getId()+"."+" "+attachment.getName());
            }}
            if (email.getTagovi()!=null) {
                for (Tag tags : email.getTagovi()) {
                    prikaz.add(tags.getId() + "." + " " + tags.getName());
                }
            }

            else{
                prikaz.add("");
            }

        ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,prikaz);
        listaA.setAdapter(arrayAdapter);
*/

    }

    public static String stringDatum(Date date) {
        TimeZone tz = TimeZone.getTimeZone("GMT+1");
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        df.setTimeZone(tz);
        return df.format(date);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.email, menu);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


    /*    if (id == R.id.action_settings) {
            startActivity(new Intent(EmailActivity.this,SettingsActivity.class));

        } */
        if(id == R.id.action_replay){
            startActivity(new Intent(EmailActivity.this,CreateEmailActivity.class));
            Intent intent = new Intent(EmailActivity.this,CreateEmailActivity.class);
            intent.putExtra("emailNaslov",naslov);
            intent.putExtra("email",emailKor);
            intent.putExtra("action","reply");
            startActivity(intent);

        }
        if(id == R.id.action_cancel){
            startActivity(new Intent(EmailActivity.this,CreateEmailActivity.class));
        }
        if(id == R.id.action_delete){

            brisanjeEmaila();

        }
        if(id == R.id.action_forward) {
            startActivity(new Intent(EmailActivity.this, CreateEmailActivity.class));
            Intent intent = new Intent(EmailActivity.this, CreateEmailActivity.class);
            intent.putExtra("emailNaslov", naslov);
            intent.putExtra("emailSadrzaj", sadrzaj);
            intent.putExtra("action", "forward");
            startActivity(intent);
        }
        if(id == R.id.action_cancel) {


        }



        return super.onOptionsItemSelected(item);
    }

    private void brisanjeEmaila() {
        AlertDialog.Builder builder = new AlertDialog.Builder(EmailActivity.this);
        builder.setTitle("Brisanje Emaila");
        builder.setMessage("Da li ste sigurni?");
        builder.setPositiveButton("Da", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = getIntent();
                Email email = (Email) intent.getSerializableExtra("email");

                Call<Void> call = ServiceClass.emailService.deleteEmail(email.getId());

                call.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        Intent intent1 = getIntent();
                        Email email = (Email) intent1.getSerializableExtra("email");
                        Intent intent = new Intent(EmailActivity.this, EmailsActivity.class);
                        intent.putExtra("PrethodniEmail", email);
                        startActivity(intent);
                        android.widget.Toast.makeText(getApplicationContext(), "Uspesno obrisan email!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        android.widget.Toast.makeText(getApplicationContext(), "Doslo je do greske!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        builder.setNegativeButton("Ne", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });

        AlertDialog ad = builder.create();
        ad.show();
    }

    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
