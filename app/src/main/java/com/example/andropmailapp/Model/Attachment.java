package com.example.andropmailapp.Model;
import android.util.Base64;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("serial")
public class Attachment implements Serializable {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("mime_type")
    @Expose
    private String mime_type;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("message")
    @Expose
    private Email message;

    @SerializedName("data")
    @Expose
    private String data;


    public Attachment() {
    }

    public Attachment(int id, String mime_type, String name, Email message, String data) {
        this.id = id;
        this.mime_type = mime_type;
        this.name = name;
        this.message = message;
        this.data = data;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMime_type() {
        return mime_type;
    }

    public void setMime_type(String mime_type) {
        this.mime_type = mime_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Email getMessage() {
        return message;
    }

    public void setMessage(Email message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
