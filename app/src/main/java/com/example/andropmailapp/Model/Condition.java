package com.example.andropmailapp.Model;

import java.io.Serializable;

public enum Condition implements Serializable {
    TO,
    FROM,
    CC,
    SUBJECT;

    public String getStatus() {
        return this.name();
    }
}
