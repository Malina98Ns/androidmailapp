package com.example.andropmailapp.Servis;

import com.example.andropmailapp.Model.Folder;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface FolderService {

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })

    @GET(ServiceClass.FOLDERSUSER)
    Call<List<Folder>> getFolders(@Path("username") String username);

    @POST(ServiceClass.FOLDERADD)
    Call<Folder> createFolder(@Body Folder folder, @Path("username") String username);


}
