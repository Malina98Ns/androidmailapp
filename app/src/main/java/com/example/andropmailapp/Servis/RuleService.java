package com.example.andropmailapp.Servis;

import com.example.andropmailapp.Model.Rule;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface RuleService {

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })

//    @GET(ServiceClass.RULES)
    Call<List<Rule>> getRules();

    @POST(ServiceClass.RULEADD)
    Call<Rule> createRule(@Body Rule rule);

}
